# coding=utf-8

"""Main bot entrypoint"""

import logging
import sys

import asyncio
import datetime
import discord
import yaml

logging.basicConfig(filename='/var/log/gentlebot.txt', filemode='a', level=logging.INFO)
logger = logging.getLogger(__name__)

try:
    with open('../config.yaml', 'r', encoding='utf-8') as cfg_file:
        config = yaml.safe_load(cfg_file.read())
except Exception as e:  # pylint: disable=broad-exception-caught
    logger.exception("Unable to load config. Please check the documentation.")
    sys.exit(1)

logger.debug(config)
TOKEN = config['token']
HEATHCLIF_CHANNEL_ID = config['channel_ids']['heathcliff']
CAH_CHANNEL_ID = config['channel_ids']['calvin_and_hobbes']

class GentleBot(discord.Client):
    """Main Gentlebot class"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bg_tasks = [
            self.loop.create_task(self.heathcliff()),
            self.loop.create_task(self.calvin_and_hobbes())
        ]

    async def on_ready(self):
        """Callback when client is logged in"""
        logger.info('Logged on as %s', self.user)

    async def calvin_and_hobbes(self):
        """Posts daily Calvin and Hobbes strips"""
        await self.wait_until_ready()
        channel = self.get_channel(CAH_CHANNEL_ID)
        while not self.is_closed():
            now = datetime.datetime.now()
            tomorrow = datetime.datetime(
                year=now.year, month=now.month, day=now.day
            ) + datetime.timedelta(days=1, hours=3)  # 3 am tomorrow
            await asyncio.sleep((tomorrow - now).seconds)
            comic_time = datetime.date.today()
            url = f"https://gocomics.com/CalvinAndHobbes/{comic_time.year}/{comic_time.month}/{comic_time.day}"
            await channel.send(url)
            await asyncio.sleep(60)

    async def heathcliff(self):
        """Posts daily heathcliff strips"""
        await self.wait_until_ready()
        channel = self.get_channel(HEATHCLIF_CHANNEL_ID)
        while not self.is_closed():
            # Calculate how long to wait
            now = datetime.datetime.now()
            tomorrow = datetime.datetime(year=now.year, month=now.month, day=now.day) + datetime.timedelta(days=1, hours=3)  # 3:00 am tomorrow.
            # Sleep for the difference.
            logger.debug('Sleeping for %d seconds... Goodnight!', (tomorrow - now).seconds)
            await asyncio.sleep((tomorrow - now).seconds)
            comic_time = datetime.date.today()
            if comic_time.month == 4 and comic_time.day == 1:
                # April Fools! You're getting garf'd
                url = f"https://gocomics.com/garfield/{comic_time.year}/4/1"
            else:
                url = f"https://gocomics.com/heathcliff/{comic_time.year}/{comic_time.month}/{comic_time.day}"
            await channel.send(url)
            await asyncio.sleep(60)  # Sleep for a minute to avoid double-posting.

client = GentleBot()
client.run(TOKEN)
        